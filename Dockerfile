FROM rootproject/root-conda:6.20.00
COPY . /tutorial2020
WORKDIR /tutorial2020/
RUN echo ">>> Compile analysis executable ..." &&  \
cd analysis && \
COMPILER=$(root-config --cxx) &&  \
FLAGS=$(root-config --cflags --libs) &&  \
$COMPILER -g -std=c++11 -O3 -Wall -Wextra -Wpedantic -o analysis analysis.cxx $FLAGS
